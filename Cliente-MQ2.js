/*dependencias*/
var Gpio = require('onoff').Gpio;
mq2 = new Gpio(5, 'in', 'both');
var count= 0;

function exit() {
  mq2.unexport();
  process.exit();
}

module.exports.AdministrarMQ2 = function(ws,msg) {
  socket = ws;

  switch (msg) {
    case "true":
      socket.emit('statusMQ2','green');
      console.log('Mensaje MQ2 recibido: ' + msg);
      console.log("Mensaje MQ2 enviado: green");
      mq2.watch(function(err, value) {
        if (err) exit();

        if (count > 50){
          msg = '¡Peligro! Hay una fuga de gas';
          socket.emit('mensaje-correo',msg);
          console.log(msg);
        }
        count++;
      });
      break;
    case "false":
      socket.emit('statusMQ2','red');
      console.log('Mensaje MQ2 recibido: ' + msg);
      console.log("Mensaje MQ2 enviado: red");
      mq2.unwatchAll();
      /*mq2.unwatch(function(err, value) {
        ws.emit('statusMQ2','red');
        console.log('Sensor MQ2 desactivado.');
      });*/
      break;
  }

}

console.log('RaspberryPI 3 Cliente-MQ2 Iniciando...');
