/*dependencias*/
var Gpio = require('onoff').Gpio;
caudal = new Gpio(22, 'in', 'both');
var count= 0;
var log = 0;

module.exports.AdministrarCaudal = function(ws,msg){
  socket = ws;

  switch (msg) {
    case "true":
      socket.emit('statusCaudal','green');
      console.log('Mensaje Caudal recibido: ' + msg);
      console.log("Mensaje Caudal enviado: green");

      caudal.watch(function(err, value) {
        if (err) exit();
        count++;
        // Revol/Lt = 600
        if(count-log>100){
          console.log('Caudal en litros: '+ (count/100) + 'Lt' +' Caudal en cm3: '+ count/1000 +'cm3');
          socket.emit("mensaje-correo","Caudal: " + count/100);
          log = 1;
        }});
      break;
    case "false":
      socket.emit('statusCaudal','red');
      console.log('Mensaje Caudal recibido: ' + msg);
      console.log("Mensaje Caudal enviado: red");

      caudal.unwatchAll();
      /*caudal.unwatch(function(err, value) {
        socket.emit('statusAGUA','false');
        console.log('Sensor Caudal Activado.');
      });*/
      break;
    }

}

console.log('RaspberryPI 3 Cliente-caudal Iniciando...');
