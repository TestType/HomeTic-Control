/*dependencias*/
var Gpio = require('onoff').Gpio;
pir = new Gpio(17, 'in', 'both');

function exit() {
  pir.unexport();
  process.exit();
}

 /*funcion que permite observar la entrada del pin de datos de PIR */
module.exports.AdministrarPIR = function(ws,msg) {
  socket = ws;
  switch (msg) {
    case "true":
      socket.emit("statusPIR", "green");
      socket.emit("mensaje-correo","Intruso detectado");
      console.log('Mensaje recibido: ' + msg);
      console.log("Mensaje enviado: green");
      /*inicia la funcion de detección*/
      pir.watch(function(err, value) {
          if (err) exit();
            console.log('Intruso detectado: '+ new Date().toTimeString() + value);
          });
          break;

    case "false":
    socket.emit("statusPIR", "red");
    console.log('Mensaje recibido: ' + msg);
    console.log("Mensaje enviado: red");
    /*detiene la funcion de detección*/
    pir.unwatchAll();
    /*pir.unwatch(function(err, value) {
        console.log('Sensor pir desactivado : '+ new Date().toTimeString() + value);
    });*/
    break;
  }
}

console.log('RaspberryPI 3 Cliente-PIR Iniciando...');
