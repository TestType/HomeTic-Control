/*dependencias*/
var GPIO = require('onoff').Gpio;
var moto = new GPIO(27, 'out');
moto.writeSync(1);


/*funcion del socket el cual cambia de estado al led*/
module.exports.AdministrarMOTO = function(ws,msg){
  socket = ws;

  switch (msg) {
    case "true":
      moto.writeSync(0);
      socket.emit('statusMOTO','green');
      console.log('Mensaje MOTO recibido: ' + msg);
      console.log("Mensaje MOTO enviado: green");
      break;

    case "false":
      moto.writeSync(1);
      socket.emit('statusMOTO','red');
      console.log('Mensaje MOTO recibido: ' + msg);
      console.log("Mensaje MOTO enviado: red");
      break;
    default:

  }

}

console.log('RaspberryPI 3 Cliente-Motobomba Iniciando...');
