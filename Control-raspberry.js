/*dependencias*/
var io = require('socket.io-client');
var socket = io.connect('https://server-socket.herokuapp.com', {reconnect: true});
var pir = require('./Cliente-PIR');
var mq2 = require('./Cliente-MQ2');
var led = require('./Cliente-LED');
var caudal = require('./Cliente-caudal');
var moto = require('./Cliente-MOTO');


socket.on('SensorPIR', function(msg) {
  pir.AdministrarPIR(socket,msg);
});

socket.on('SensorMQ2', function(msg) {
  mq2.AdministrarMQ2(socket,msg);
});

socket.on('LED', function(msg) {
  led.AdministrarLED(socket,msg);
});

socket.on('SensorCaudal', function(msg) {
  caudal.AdministrarCaudal(socket,msg);
});

socket.on('SensorMoto', function(msg) {
  moto.AdministrarMOTO(socket,msg);
});

console.log('RaspberryPI 3 Control Iniciando...');
