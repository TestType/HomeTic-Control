/*dependencias*/
var GPIO = require('onoff').Gpio;
var led = new GPIO(23, 'out');
led.writeSync(1);


/*funcion del socket el cual cambia de estado al led*/
module.exports.AdministrarLED = function(ws,msg){
  socket = ws;

  switch (msg) {
    case "true":
      led.writeSync(0);
      socket.emit('statusLED','green');
      socket.emit("mensaje-correo","Luces Encendidas");
      console.log('Mensaje LED recibido: ' + msg);
      console.log("Mensaje LED enviado: green");
      break;

    case "false":
      led.writeSync(1);
      socket.emit('statusLED','red');
      socket.emit("mensaje-correo","Luces Apagadas");
      console.log('Mensaje LED recibido: ' + msg);
      console.log("Mensaje LED enviado: red");
      break;
    default:

  }

}

console.log('RaspberryPI 3 Cliente-LED Iniciando...');
